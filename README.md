# BuddyBot for Childern
An open source, That will identify the facial expression, alert the parent and that will also mask the word, site blocking/filtering extension for Google Chrome. Version 0.3.

## Dependencies
- Java latest version
- python 3.8.6 
- pip 20.3.1
- Google Chrome Browser

## How to install?
- Go to the Java Downloads page using this [link](https://www.java.com/en/download/) 
- Click agree and start free download to start the download. 
- Start the Java installer by double-clicking the installer's icon or file name in the download location.
- Then follow the instructions provided by the Installation wizard to install java.
- Download and install python 3.8.6 and pip , from this [link](https://www.python.org/downloads/release/python-386/)
- Download “Windows x86-64 executable installer” and double click the file after downloading to start the installation. (ensure that pip checkbox is checked during the installation steps)
- Ensure that add python3.8 to PATH has been checked
- Click install now and the python will be installed
- Download the v0.3 zip file from the [link](https://s3.ap-south-1.amazonaws.com/buddybot.hivericks/static/v0.3.zip)
- From the downlad location , extract the zip to find 2 folder namely Google chrome extension and core
- The Google chrome extension has the files related to the chrome extension module
- Core has Buddy bot core application installer

# Chrome Extension

## Usage
- Go to chrome://extensions/ in your browser
- Click on "Load unpacked" and point to the directory in which all of the files are located.

## Features
- By default, blocking access to www.facebook.com, www.twitter.com, www.instagram.com, and www.youtube.com.
- You can view, add and delete URLs (including above) from the filtering list, by clicking on the "Show filter list" in the extension context menu (right-click on the extension icon).
- By left clicking on the icon, you can toggle the blocking functionality. The icon will appear red if filtering is turned on, and green otherwise.
- By picking "Block this page" from the context menu, you can add the current page to the filter. This can be accessed by right-clicking on any web page and picking the right option as well.
- In the options tab, it is possible to choose the extension's behaviour when it encounters blocked site (closing the tab or clearing it).
- You can set up a timer, that will switch the blocking on for a certain amount of time. During that time, it will be impossible to toggle filtering off or change the duration. You can find timer settings in "Timer mode setup" tab of the menu.
- If you happen to reinstall the extension, the list your blocked sites will be kept safe and waiting. You can then decide if you want to apply it or not.

# BuddyBot Core

## Usage
- Go to the core folder that is extracted to find installer.exe file.
- Double click the installer.exe file , it will ask for adminstrator permission , press allow and it will start downloading the BuddyBot core
- After the installer finished running, the BuddyBot core opens.
- Enter the email id in the field provided and then press GET OTP button. 
- Enter the OTP obtained from the email in the OTP field and then press submit.
- Then a new window will appear , with two buttons learn and emotion detect.

## Features
- The Learn button will provide an exercise to train the user facial emotion for an accurate model.
- The emotion detect will monitor the child for stressful behaviour and will send out an email alert to the child's parent if the child is stressed for defined time interval.



# Internal Documents, Repository and Test Results.
+ [Backend](https://bitbucket.org/arunskg/buddybot-django-backend)   - Django Server that will Provide the authentication, Communication with Client Application (Core).
+ [Core](https://bitbucket.org/arunskg/buddybot-core) - The core is run in java that will handle all business rules in the Client Computer.
+ [Documentation](https://bitbucket.org/arunskg/buddybot-design-documents) - This will have full system Design Documents.
+ [Machine Learning](https://bitbucket.org/arunskg/buddybot-expression-detection-ml-backend) - THis will have the Facial Expression Ideantification Learning Module.
+ [Test Results](https://bitbucket.org/arunskg/buddybot-test-report) - All the test results will be documented in here. 
+ [Chrome Extension](https://bitbucket.org/arunskg/buddybot-chrome-extension)  Chrome Extension  


## Feedback
For all the feedback feel free to email support support@hivericks.zohodesk.com

## Developed By
[![N|](https://www.hivericks.com/WiSilica-RGB-Web-Logo-LG.png)](https://hivericks.com)