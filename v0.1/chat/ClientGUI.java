import javax.swing. * ;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import sun.util.calendar.BaseCalendar.Date;

import java.awt. * ;
import java.awt.event. * ;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/*
 * The Client with its GUI
 */
public class ClientGUI extends JFrame implements ActionListener {

  private static final long serialVersionUID = 1L;
  private JLabel label;
  private JTextField tf;
  private JTextField tfServer,
  tfPort;
  private JButton login,
  logout,
  showAll,
  Report;
  private JTextArea ta;
  private boolean connected;
  private Client client;
  private int defaultPort;
  private String defaultHost;

  ClientGUI(String host, int port) {

    super("Chat Application: Client");
    defaultPort = port;
    defaultHost = host;

    JPanel northPanel = new JPanel(new GridLayout(3, 1));
    JPanel serverAndPort = new JPanel(new GridLayout(1, 5, 1, 3));

    tfServer = new JTextField(host);
    tfPort = new JTextField("" + port);
    tfPort.setHorizontalAlignment(SwingConstants.RIGHT);

    serverAndPort.add(new JLabel("Server Address :  "));
    serverAndPort.add(tfServer);
    serverAndPort.add(new JLabel("Port Number :  "));
    serverAndPort.add(tfPort);
    serverAndPort.add(new JLabel(""));
    serverAndPort.hide();
    northPanel.add(serverAndPort);

    label = new JLabel("Enter your username", SwingConstants.CENTER);
    northPanel.add(label);
    tf = new JTextField("User");
    tf.setBackground(Color.WHITE);
    northPanel.add(tf);
    add(northPanel, BorderLayout.NORTH);

    ta = new JTextArea("Welcome to the chat room\n", 80, 80);
    JPanel centerPanel = new JPanel(new GridLayout(1, 1));
    centerPanel.add(new JScrollPane(ta));
    ta.setEditable(false);
    add(centerPanel, BorderLayout.CENTER);

    login = new JButton("Login");
    login.addActionListener(this);
    logout = new JButton("Logout");
    logout.addActionListener(this);
    logout.setEnabled(false);
    showAll = new JButton("Show all users");
    showAll.addActionListener(this);
    showAll.setEnabled(false);
    Report = new JButton("Report");
    Report.addActionListener(this);
    Report.setEnabled(false);
    JPanel southPanel = new JPanel();
    southPanel.add(login);
    southPanel.add(logout);
    southPanel.add(showAll);
    southPanel.add(Report);
    add(southPanel, BorderLayout.SOUTH);

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setSize(600, 600);
    setVisible(true);
    tf.requestFocus();

  }

  void append(String str) {
    //String a = "\"<html>"+str+" <img src=\\\"file:someFile.jpg\\\">after</html>\"";
    ta.append(str);

    ta.setCaretPosition(ta.getText().length() - 1);
  }

  void connectionFailed() {
    login.setEnabled(true);
    logout.setEnabled(false);
    showAll.setEnabled(false);
    Report.setEnabled(false);
    label.setText("Enter your username");
    tf.setText("User");
    tfPort.setText("" + defaultPort);
    tfServer.setText(defaultHost);
    tfServer.setEditable(false);
    tfPort.setEditable(false);
    tf.removeActionListener(this);
    connected = false;
  }

  void game(String a) {
    String directory = System.getProperty("user.home") + File.separator + "Documents" + File.separator + "Hive";
    String fileName = "data.json";
    String absolutePath = directory + File.separator + fileName;
    System.out.println("Enter");
    // Write the content in file 
    File theDir = new File(directory);
    if (!theDir.exists()) {
      theDir.mkdirs();
    }
    File f = new File(directory, fileName);
    if (f.exists()) {
      System.out.println("Enter-exist");
      try (BufferedReader br = new BufferedReader(new FileReader(absolutePath))) {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
          sb.append(line);
          sb.append(System.lineSeparator());
          line = br.readLine();
        }
        String everything = sb.toString();
        System.out.println(everything);
        JSONObject item = (JSONObject) JSONValue.parse(everything);
        if (a == "badword") {
          item.put("badword", "true");
          try (FileWriter fileWriter = new FileWriter(absolutePath)) {
            String fileContent = item.toString();
            fileWriter.write(fileContent);
            fileWriter.close();

          } catch(IOException e) {
            System.out.println(e);
            // Cxception handling

          }
          return;
        }
        if (a == "login") {
          item.put("loginTime", timer(a, "-"));
          item.put("badword", "false");
          try (FileWriter fileWriter = new FileWriter(absolutePath)) {
            String fileContent = item.toString();
            fileWriter.write(fileContent);
            fileWriter.close();
          } catch(IOException e) {
            System.out.println(e);
            // Cxception handling
          }
        }
        if (a == "logout") {
          System.out.println("lgot" + item.get("badword") == "false");
          if ("false".equals(item.get("badword")) ) {
            System.out.println("lgot f");
            String res = timer(a, item.get("loginTime").toString());
            int a1 = Integer.parseInt(item.get("points").toString()) + Integer.parseInt(res);
            item.put("points", a1);
            JOptionPane.showMessageDialog(null, "Congrats you have won " + res + " points!!!");
          }
          try (FileWriter fileWriter = new FileWriter(absolutePath)) {
            String fileContent = item.toString();
            fileWriter.write(fileContent);
            fileWriter.close();
          } catch(IOException e) {
            System.out.println(e);
            // Cxception handling
          }
        }

      } catch(FileNotFoundException e) {

        // Exception handling
      } catch(IOException e) {
        // Exception handling
      }
    } else {
      System.out.println("Enter-no");

      try (FileWriter fileWriter = new FileWriter(absolutePath)) {

        String fileContent = "{}";
        fileWriter.write(fileContent);
        // fileWriter.close();
        if (a == "login") {
          JSONObject item = (JSONObject) JSONValue.parse("{}");
          item.put("loginTime", timer(a, "-"));
          item.put("badword", "false");
          fileWriter.write(item.toString());
          fileWriter.close();
        }
      } catch(IOException e1) {

}
      try (BufferedReader br = new BufferedReader(new FileReader(absolutePath))) {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
          sb.append(line);
          sb.append(System.lineSeparator());
          line = br.readLine();
        }
        String everything = sb.toString();
        System.out.println(everything);
      } catch(FileNotFoundException e) {
        System.out.println(e);
        // Exception handling
      } catch(IOException e) {
        System.out.println(e);
        // Exception handling
      }
    }

  }
  String timer(String Status, String time) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
    Calendar calendar = Calendar.getInstance();
    if (Status == "login") {
      System.out.println(dateFormat.format(calendar.getTime()));
      return dateFormat.format(calendar.getTime()).toString();
    }
    if (Status == "logout") {
      System.out.println(dateFormat.format(calendar.getTime()));

      try {
        java.util.Date date1 = dateFormat.parse(time);
        String timeNow = dateFormat.format(calendar.getTime());
        java.util.Date date2 = dateFormat.parse(timeNow);
        long difference = date2.getTime() - date1.getTime();
        String out = Long.toString(difference);
        System.out.println(difference);

        return out;
      } catch(ParseException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }
    return null;

  }

  /*
	* Button or JTextField clicked
	*/
  public void actionPerformed(ActionEvent e) {
    Object o = e.getSource();
    System.out.println("Report Action");
    if (o == logout) {
      game("logout");
      client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, ""));
      return;
    }
    if (o == showAll) {
      client.sendMessage(new ChatMessage(ChatMessage.SHOWALL, ""));
      return;
    }
    if (o == Report) {
      //client.sendMessage(new ChatMessage(ChatMessage.SHOWALL, ""));	
      System.out.println("Report");
      String resp = "Enter the Word to report";

      String input = JOptionPane.showInputDialog(null, resp);

      if (input.isEmpty()) JOptionPane.showMessageDialog(null, "Field is Empty");
      else JOptionPane.showMessageDialog(null, "Successfully submitted");

      //JOptionPane.showMessageDialog(null, "Report");
      return;
    }
    if (connected) {
      String output = BadWordFilter.getCensoredText(tf.getText());
      System.out.println(output);
      if (output != "false") {
        client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, tf.getText()));
        tf.setText("");
      }
      else {
        game("badword");
        tf.setText("");
        JOptionPane.showMessageDialog(null, "Previous Message is blocked due to inappropriate words used in conversation");
      }
      return;
    }

    if (o == login) {
      game("login");
      String username = tf.getText().trim();
      if (username.length() == 0) return;
      String server = tfServer.getText().trim();
      if (server.length() == 0) return;
      String portNumber = tfPort.getText().trim();
      if (portNumber.length() == 0) return;
      int port = 0;
      try {
        port = Integer.parseInt(portNumber);
      }
      catch(Exception en) {
        return;
      }

      client = new Client(server, port, username, this);
      if (!client.start()) return;
      tf.setText("");
      label.setText("Enter your message below. Press ENTER to send.");
      connected = true;

      login.setEnabled(false);
      logout.setEnabled(true);
      showAll.setEnabled(true);
      Report.setEnabled(true);
      tfServer.setEditable(false);
      tfPort.setEditable(false);
      tf.addActionListener(this);
    }

  }

  public static void main(String[] args) {
    new ClientGUI("35.170.187.22", 1000);
  }

}